
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

class Student implements Comparable<Student> {

    int id;
    String name;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student(String searchName) {
        this.name = searchName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
//        public static Comparator<Student> comp1 = new Comparator<Student>(){
//            public int compare(Student s1  , Student s2){
//                return s1.getId().compa
//            }
//        }

    @Override
    public int compareTo(Student s) {
        int compareId = s.getId();
        return this.id - compareId;
    }

    public static Comparator<Student> StudentNameComparator = new Comparator<Student>() {

        public int compare(Student s1, Student s2) {
            String name1 = s1.getName().toUpperCase();
            String name2 = s2.getName().toUpperCase();
            return name1.compareTo(name2);

        }

    };

}

public class Binarysearch {

    public static void main(String[] args) {
        int n, id, searchId;
        String name, type, searchName;
        Scanner kb = new Scanner(System.in);
        n = kb.nextInt();
        Student[] s = new Student[n];
        for (int i = 0; i < n; i++) {
            id = kb.nextInt();
            name = kb.next();
            s[i] = new Student(id, name);
        }
        type = kb.next();
        if (type.equals("id")) {
            searchId = kb.nextInt();
            Arrays.sort(s);
            if (Arrays.binarySearch(s, new Student(searchId, null)) >= 0) {
                System.out.println("found");
            } else {
                System.out.println("not found");
            }
        } 
//        else if (type.equals("name")) {
//            searchName = kb.next();
//            Arrays.sort(s,Student.StudentNameComparator);
//            System.out.println(Arrays.binarySearch(s, new Student(searchName)));
//            
//            if (Arrays.binarySearch(s, new Student(searchName)) >= 0) {
//                System.out.println("found");
//            } else {
//                System.out.println("not found");
//            }
//        }
            for (Student a : s) {
                System.out.println(a.getId());
            }
        }
    }
